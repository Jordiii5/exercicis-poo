class Aliments{
    var nom = ""
    var preu = 0
    var pes = 0
    var calories = 0

    fun mostrarInventariAliments(){
        println("El producte $nom, costa $preu €, pesa $pes g i conté aproximadament $calories cal")
    }
}

class Begudes{
    var nom = ""
    var preu = 0.0
    var incrementBegudes = false

    fun mostrarInventariBegudes(){
        if(incrementBegudes==false){
            println("El producte $nom, costa $preu €")
        }else println("El producte $nom, costa ${preu + (preu*0.10)} €")
    }
}



fun main(){
    var croissant = Aliments()
    croissant.nom = "Croissant"
    croissant.preu = 2
    croissant.pes = 50
    croissant.calories = 120
    croissant.mostrarInventariAliments()

    var ensaimada = Aliments()
    ensaimada.nom = "Ensaimada"
    ensaimada.preu = 4
    ensaimada.pes = 88
    ensaimada.calories = 200
    ensaimada.mostrarInventariAliments()

    var donut = Aliments()
    donut.nom = "Donut"
    donut.preu = 3
    donut.pes = 55
    donut.calories = 150
    donut.mostrarInventariAliments()

    var aigua = Begudes()
    aigua.nom = "Aigua"
    aigua.preu = 1.00
    aigua.incrementBegudes = false
    aigua.mostrarInventariBegudes()

    var cafeTallat = Begudes()
    cafeTallat.nom = "Cafe Tallat"
    cafeTallat.preu = 1.35
    cafeTallat.incrementBegudes = false
    cafeTallat.mostrarInventariBegudes()

    var teVermell = Begudes()
    teVermell.nom = "Té Vermell"
    teVermell.preu = 1.50
    teVermell.incrementBegudes = false
    teVermell.mostrarInventariBegudes()

    var cola = Begudes()
    cola.nom = "Cola"
    cola.preu = 1.65
    cola.incrementBegudes = true
    cola.mostrarInventariBegudes()

}
