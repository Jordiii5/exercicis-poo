class MechanicalArm {
    var openAngle: Double = 0.0
    var altitude: Double = 0.0
    var turnedOn: Boolean = false

    fun toggle() {
        turnedOn = !turnedOn
    }

    fun updateAltitude(altitudeChange: Double) {
        if (!turnedOn) return

        altitude += altitudeChange
        if (altitude < 0) altitude = 0.0
        if (altitude > 30) altitude = 30.0
    }

    fun updateAngle(angleChange: Double) {
        if (!turnedOn) return

        openAngle += angleChange
        if (openAngle < 0) openAngle += 360
        if (openAngle >= 360) openAngle -= 360
    }

    override fun toString(): String {
        return "MechanicalArm{openAngle=$openAngle, altitude=$altitude, turnedOn=$turnedOn}"
    }
}

fun main() {
    val arm = MechanicalArm()

    arm.toggle()
    println(arm)

    arm.updateAltitude(3.0)
    println(arm)

    arm.updateAngle(180.0)
    println(arm)

    arm.updateAltitude(-3.0)
    println(arm)

    arm.updateAngle(-180.0)
    println(arm)

    arm.updateAltitude(3.0)
    println(arm)

    arm.toggle()
    println(arm)
}
