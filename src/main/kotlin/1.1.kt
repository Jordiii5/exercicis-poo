class Pastisseria{
    var nom = ""
    var preu = 0
    var pes = 0
    var calories = 0

    fun mostrarInventari(){
        println("El producte $nom, costa $preu €, pesa $pes g i conté aproximadament $calories cal")
    }
}

fun main(){
    var croissant = Pastisseria()
    croissant.nom = "Croissant"
    croissant.preu = 2
    croissant.pes = 50
    croissant.calories = 120
    croissant.mostrarInventari()

    var ensaimada = Pastisseria()
    ensaimada.nom = "Ensaimada"
    ensaimada.preu = 4
    ensaimada.pes = 88
    ensaimada.calories = 200
    ensaimada.mostrarInventari()

    var donut = Pastisseria()
    donut.nom = "Donut"
    donut.preu = 3
    donut.pes = 55
    donut.calories = 150
    donut.mostrarInventari()
}
