import java.util.Scanner

class Taulell(preuUnitari:Int, llargada:Int, amplada:Int){
    var totalPreuT = 0
    init{
        totalPreuT = preuUnitari * llargada * amplada
    }
}

class Llisto(preuUnitari:Int, llargada:Int){
    var totalPreuL = 0
    init{
        totalPreuL = preuUnitari * llargada
    }
}

fun main(){
    var totalPreu = 0
    print("Introdueix el nombre d'elements:")
    val scanner = Scanner(System.`in`)
    for (i in 1..scanner.nextInt()){
        val nom = scanner.next()

        if (nom == "Taulell"){
            val preuUnitari = scanner.nextInt()
            val llargada = scanner.nextInt()
            val amplada = scanner.nextInt()

            val taulell = Taulell(preuUnitari,llargada, amplada)

            totalPreu += taulell.totalPreuT
        }
        if (nom == "Llistó"){
            val preuUnitari = scanner.nextInt()
            val llargada = scanner.nextInt()

            val llisto = Llisto(preuUnitari,llargada)

            totalPreu += llisto.totalPreuL
        }
    }
    println("El preu total és: $totalPreu€")
}
