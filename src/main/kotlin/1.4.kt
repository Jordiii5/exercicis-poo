import java.util.Scanner
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"
class LampadaAliBaba (identificador : String) {
    var nom = identificador
    var color = 0
    val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    private var intensitatPujada=true
    private var offColor = 1
    var intensitat = 0
    init {
        println(" Lampada: ${nom}\nColor: ${colors[color]}  $ANSI_RESET - intensitat $intensitat ")
    }

    fun encendre (){
        if(color==0){
            color=offColor
            intensitat=1
            intensitatPujada=true
        }
    }
    fun apagar (){
        if(color>0){
            offColor=color
            color=0
            intensitat=0
        }
    }
    fun canviarColor (){
        if (color!=colors.size-1 && color != 0) color++
        else if(color==colors.size-1 && color != 0) color=1
    }
    fun canviarIntensitat() {
        if (intensitatPujada && color != 0 && intensitat!=5) intensitat++
        else if (intensitatPujada && intensitat==5 ){
            intensitatPujada=false
            intensitat--
        }
        else if (!intensitatPujada && intensitat>1) intensitat--
        else if (!intensitatPujada && intensitat==1 && color != 0){
            intensitatPujada = true
            intensitat++
        }
    }

}
fun main () {
    val scanner = Scanner(System.`in`)
    val lampadaMenjador = LampadaAliBaba("Menjador")
    val lampadaCuina = LampadaAliBaba("Cuina")
    lampadaMenjador.encendre()
    println("\n Lampada: ${lampadaMenjador.nom}\nColor: ${lampadaMenjador.colors[lampadaMenjador.color]}  $ANSI_RESET - intensitat ${lampadaMenjador.intensitat} ")
    repeat(3){lampadaMenjador.canviarColor()}
    println(" Lampada: ${lampadaMenjador.nom}\nColor: ${lampadaMenjador.colors[lampadaMenjador.color]}  $ANSI_RESET - intensitat ${lampadaMenjador.intensitat} ")
    do {
        lampadaMenjador.canviarIntensitat()
    }while (lampadaMenjador.intensitat != 5)
    println(" Lampada: ${lampadaMenjador.nom}\nColor: ${lampadaMenjador.colors[lampadaMenjador.color]}  $ANSI_RESET - intensitat ${lampadaMenjador.intensitat} ")
    lampadaCuina.encendre()
    println("\n Lampada: ${lampadaCuina.nom}\nColor: ${lampadaCuina.colors[lampadaCuina.color]}  $ANSI_RESET - intensitat ${lampadaCuina.intensitat} ")
    repeat(2) {lampadaCuina.canviarColor()}
    println(" Lampada: ${lampadaCuina.nom}\nColor: ${lampadaCuina.colors[lampadaCuina.color]}  $ANSI_RESET - intensitat ${lampadaCuina.intensitat} ")
    do {
        lampadaCuina.canviarIntensitat()
    }while (lampadaCuina.intensitat != 5)
    println(" Lampada: ${lampadaCuina.nom}\nColor: ${lampadaCuina.colors[lampadaCuina.color]}  $ANSI_RESET - intensitat ${lampadaCuina.intensitat} ")
    lampadaCuina.apagar()
    println(" Lampada: ${lampadaCuina.nom}\nColor: ${lampadaCuina.colors[lampadaCuina.color]}  $ANSI_RESET - intensitat ${lampadaCuina.intensitat} ")
    lampadaCuina.canviarColor()
    println(" Lampada: ${lampadaCuina.nom}\nColor: ${lampadaCuina.colors[lampadaCuina.color]}  $ANSI_RESET - intensitat ${lampadaCuina.intensitat} ")
    lampadaCuina.encendre()
    println(" Lampada: ${lampadaCuina.nom}\nColor: ${lampadaCuina.colors[lampadaCuina.color]}  $ANSI_RESET - intensitat ${lampadaCuina.intensitat} ")
    lampadaCuina.canviarColor()
    println(" Lampada: ${lampadaCuina.nom}\nColor: ${lampadaCuina.colors[lampadaCuina.color]}  $ANSI_RESET - intensitat ${lampadaCuina.intensitat} ")
    do {
        lampadaCuina.canviarIntensitat()
    }while (lampadaCuina.intensitat != 5)
    println(" Lampada: ${lampadaCuina.nom}\nColor: ${lampadaCuina.colors[lampadaCuina.color]}  $ANSI_RESET - intensitat ${lampadaCuina.intensitat} ")
}

