open class Electrodomestics(val preuBase: Int, var color: String= "Blanc", val consum: String= "G", val pes: Int= 5){
    var preuFinal = 0
    init {
        preuFinal()
    }
    open fun preuFinal () {
        preuFinal += preuBase
        when (consum){
            "A" -> preuFinal += 35
            "B" -> preuFinal += 30
            "C" -> preuFinal += 25
            "D" -> preuFinal += 20
            "E" -> preuFinal += 15
            "F" -> preuFinal += 10
            "G" -> preuFinal += 0
        }
        when (pes){
            in 6..20 -> preuFinal += 20
            in 21..50 -> preuFinal += 50
            in 51..80 -> preuFinal += 80
            in 80..100000 -> preuFinal +=100
        }
    }
}

class Rentadora(preuBase: Int, color: String, consum: String, pes: Int,val carrega: Int) : Electrodomestics(preuBase, color, consum, pes){
    init {
        super.preuFinal
        preuFinal()
    }
    override fun preuFinal() {
        super.preuFinal += preuBase
        when (carrega){
            6 -> preuFinal += 55
            7 -> preuFinal += 55
            8 -> preuFinal += 70
            9 -> preuFinal += 85
            10 -> preuFinal += 100
        }
    }
}

class Televisio(preuBase: Int, color: String, consum: String, pes: Int,val tamany: Int) : Electrodomestics(preuBase, color, consum, pes){
    init {
        super.preuFinal
        preuFinal()
    }
    override fun preuFinal() {
        super.preuFinal += preuBase
                when (tamany){
            in 29..32 -> preuFinal += 50
            in 33..42 -> preuFinal += 100
            in 43..50 -> preuFinal += 150
            in 50..100000 -> preuFinal += 200
        }
    }
}

fun main(){
    val electrodomestic1=Electrodomestics(35,"blanc","D",2)
    val electrodomestic2=Electrodomestics(50,"color","C",24)
    val electrodomestic3=Electrodomestics(48,"blanc","G",32)
    val electrodomestic4=Electrodomestics(39,"platejat","E",17)
    val electrodomestic5=Electrodomestics(70,"platejat","F",83)
    val electrodomestic6=Electrodomestics(83,"blanc","A",19)
    val rentadora1=Rentadora(60,"color","B",66, 5)
    val rentadora2=Rentadora(72,"platejat","D",70, 8)
    val televisio1=Televisio(75,"platejat","C",34, 58)
    val televisio2=Televisio(60,"blanc","A",13, 28)
    val electrodomestics = mutableListOf(electrodomestic1,electrodomestic2,electrodomestic3,electrodomestic4,electrodomestic5,electrodomestic6,rentadora1,rentadora2,televisio1,televisio2)
    val preus = MutableList(6){0}
    for (i in electrodomestics){
        if (i is Rentadora || i is Televisio){
            if (i is Rentadora){
                preus[2] += i.preuBase
                preus[3] += i.preuFinal
            }else {
                preus[4] += i.preuBase
                preus[5] += i.preuFinal
            }
        }else{
            preus[0] += i.preuBase
            preus[1] += i.preuFinal

        }
    }
    println("Electrodomèstics: \n" +
            "\t -  Preu Base : ${preus[0]}€\n" +
            "\t -  Preu Final : ${preus[1]}€\n" +
            "Rentadores: \n" +
            "\t -  Preu Base : ${preus[2]}€\n" +
            "\t -  Preu Final : ${preus[3]}€\n" +
            "Televisors: \n" +
            "\t -  Preu Base : ${preus[4]}€\n" +
            "\t -  Preu Final : ${preus[5]}€\n")
}

